﻿using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;

namespace RegAdmin
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    [ProgId("RegAdmin.Link")]

    public class Registration
    {
       public static string logFile = @"C:\Touch\Logs\Licenser\" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString() + "_dll.log";
        public string getRegDetails(string cRegServer, string soapAction, string cPeodId, string cRegKey, string cEndUser, string cSoftwareVersion, string cPcKey, string cTerminalNo, string cAutomated, string cmodeSel, string cBranchNumber, string cDatetime)
        {
            string cEndUserName = cEndUser;
            try
            {

                cEndUserName = cEndUserName.Replace("&", "&amp;");
                cEndUserName = cEndUserName.Replace("'", "&apos;");
                cEndUserName = cEndUserName.Replace("<", "&lt;");
                cEndUserName = cEndUserName.Replace(">", "&gt;");

                StringBuilder xml = new StringBuilder();
                xml.Append(@"<?xml version=""1.0"" encoding=""utf-8""?>");
                xml.Append(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" ");
                xml.Append(@"xmlns:ces=""http://www.cessoftware.co.uk/"" >");
                xml.Append("<soapenv:Header/>");
                xml.Append("<soapenv:Body>");
                xml.Append("<ces:registrationCheckTouchRequest>");
                xml.Append("<ces:prodIdx>" + cPeodId + "</ces:prodIdx>");
                xml.Append("<ces:RegKey>" + cRegKey + "</ces:RegKey>");
                xml.Append("<ces:EndUserName >" + cEndUserName + "</ces:EndUserName>");
                xml.Append("<ces:softwareVersion >" + cSoftwareVersion + "</ces:softwareVersion>");
                xml.Append("<ces:PCKey>" + cPcKey + "</ces:PCKey>");
                xml.Append("<ces:TerminalNo>" + cTerminalNo + "</ces:TerminalNo>");
                xml.Append("<ces:Automated>" + cAutomated + "</ces:Automated>");
                xml.Append("<ces:modsSel>" + cmodeSel + "</ces:modsSel>");
                xml.Append("<ces:BranchNumber>" + cBranchNumber + "</ces:BranchNumber>");
                xml.Append("<ces:DateTimeCheck>" + cDatetime + "</ces:DateTimeCheck>");
                xml.Append("</ces:registrationCheckTouchRequest>");
                xml.Append("</soapenv:Body>");
                xml.Append("</soapenv:Envelope>");


                using (StreamWriter sw = File.AppendText(logFile))
                {
                    sw.WriteLine(DateTime.Now.ToString() + "Server: " + cRegServer + "SoapAction: " + soapAction);
                    sw.WriteLine(DateTime.Now.ToString() + " XML Sent: " + xml.ToString());
                    sw.WriteLine(DateTime.Now.ToString() + "");
                }

                string cResult = getRegResponse(xml.ToString(), cRegServer, soapAction);
                
                
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(cResult);
                
                
                var rs = xmlDoc.ChildNodes[1].InnerText;

                
                    using (StreamWriter sw = File.AppendText(logFile))
                    {                        
                        sw.WriteLine(DateTime.Now.ToString() + " Returning result: " + rs.ToString());
                    }
                

                   
                

                return rs.ToString();

            }
            catch (Exception ex)
            {
                    using (StreamWriter sw = File.AppendText(logFile))
                    {
                        sw.WriteLine(DateTime.Now.ToString() + " ERROR: " + ex.Message);
                    }
             

                return ex.Message; 
            }
            
        }

        public string checkTouchModule(string cRegServer, string soapAction, string cRegKey, string cPcKey, string cModuleCode)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append(@"<?xml version=""1.0"" encoding=""utf-8""?>");
            xml.Append(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" ");
            xml.Append(@"xmlns:ces=""http://tempuri.org/"" >");
            xml.Append("<soapenv:Header/>");
            xml.Append("<soapenv:Body>");
            xml.Append("<ces:checkTouchModule>");
            xml.Append("<ces:regKey>" + cRegKey + "</ces:regKey>");
            xml.Append("<ces:dongleCode>" + cPcKey + "</ces:dongleCode>");
            xml.Append("<ces:moduleCode>" + cModuleCode + "</ces:moduleCode>");
            xml.Append("</ces:checkTouchModule>");
            xml.Append("</soapenv:Body>");
            xml.Append("</soapenv:Envelope>");

            string cResult = getRegResponse(xml.ToString(), cRegServer, soapAction);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(cResult);

            var rs = xmlDoc.ChildNodes[1].InnerText;

            return rs.ToString();
        }
        //PostCaptureV2
        public string PostCaptureTouchV2(string cRegServer, string soapAction, string cXmlString)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append(@"<?xml version=""1.0"" encoding=""utf-8""?>");
            xml.Append(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" ");
            xml.Append(@"xmlns=""http://www.cessoftware.co.uk/"" >");
            xml.Append("<soapenv:Header/>");
            xml.Append("<soapenv:Body>");
            xml.Append("<PostCaptureTouchV2>");
            xml.Append("<CapturedDetails>" + cXmlString + "</CapturedDetails>");
            xml.Append("</PostCaptureTouchV2>");
            xml.Append("</soapenv:Body>");
            xml.Append("</soapenv:Envelope>");
            try
            {

                string cResult = getRegResponse(xml.ToString(), cRegServer, soapAction);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(cResult);
                var rs = xmlDoc.ChildNodes[1].InnerText;
                return rs.ToString();
            }
            catch(Exception e)
            {
                return e.Message; 
            }           
            
        }

        public static string getRegResponse(string xml, string address, string soapAction)
        {
                        
            string soapResult = "";
            HttpWebRequest request = CreateWebRequest(address, soapAction);
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(xml);

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (WebResponse response = request.GetResponse()) 
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                    Console.WriteLine(soapResult);
                }
            }
            return soapResult;
        }

        public static HttpWebRequest CreateWebRequest(string url, string soapAction)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", soapAction); // "http://www.cessoftware.co.uk/registrationCheckTouchRequest"); 
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }               
    }
}
